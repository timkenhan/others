from random import randrange
from collections import Counter


def rolldice(n=60, f=6):
	t = list()
	r = list()

	for i in range(n):
		t.append(randrange(f))
	
	s = Counter(t)
	
	for i in range(f):
		r.append(s[i])
	
	return r
