; A Simple Genetic Algorithm
; By Timothy K Handojo
; October 25, 2016
; For CS 441 Fall 2016 with B. York
; Lab 2
; Portland State University

; The main function is provided by calling:
;       (select_smallest ...   )

; Description: This program simply finds the lowest possible value
;              from output of a function, f(x, y, z), mapped from
;              distinct variables: x, y, and z, where:
;                       f(x, y, z) = (x^2) + (y^2) + (z^2) .
;
;              This lowest value is found thru means of genetic
;              programming, where each variables is represented by
;              bitstrings, and natural selection is applied.
;              The fitness is determined by simply comparing
;              the output of f(x, y, z), the lower the better.
;
;              New values are generated in two ways:
;               1. by splicing two cells together
;               2. by randomly mutating cells



(DEFPARAMETER *population* (LIST)
        "The petri dish of this experiment")

(DEFCONSTANT +template+ (MAKE-ARRAY 30 :initial-element 0)
             "Basic structure of a cell")

(DEFCONSTANT +x_index+ 0
             "x: bit 0 to 9")
(DEFCONSTANT +y_index+ 10
             "y: bit 10 to 19")
(DEFCONSTANT +z_index+ 20
             "z: bit 20 to 29")
(DEFCONSTANT +bit_len+ 10
             "each of x, y, and z has 10 bit")

(DEFCONSTANT +default_median+ 13
             "default value for argument median")
(DEFCONSTANT +default_mut_0+ 0.23
             "default value for argument mut_0")
(DEFCONSTANT +default_mut_1+ 0.53
             "default value for argument mut_1")


(DEFUN fitness (cell)
       "Fitness function"
       (LET ((x 0) (y 0) (z 0)) ; find magnitude of each variable
            (DOTIMES (i (1- +bit_len+))
                     (SETF x
                           (+ x (* (EXPT 2 i)
                                  (AREF cell (+ i +x_index+))
                     )     )    ) 
                     (SETF y
                           (+ y (* (EXPT 2 i)
                                   (AREF cell (+ i +y_index+))
                     )     )    )
                     (SETF z
                           (+ z (* (EXPT 2 i)
                                   (AREF cell (+ i +z_index+))
                     )     )    )
            ) ; their signs are irrelevant
            ; because their values will be squared
            (+ (EXPT x 2) (EXPT y 2) (EXPT z 2))
)      )


(DEFUN display_cell (cell)
       (WHEN (NOT cell) (FORMAT T "nil~%") (RETURN-FROM display_cell 0))
       (FORMAT T "#( ")
       (DOTIMES (i 30)
                (FORMAT T "~d " (AREF cell i))
       )
       (FORMAT T "); ")
       (FORMAT T "fitness: ~d~%" (fitness cell) )
       1
)


(DEFUN display_population ()
      (DOLIST (current *population*)
          (display_cell current)
      ) (FORMAT T "(~d cells)~%" (LENGTH *population*))
)


(DEFUN init_cell ()
       "Initiation of cell"
       (LET ((cell (COPY-SEQ +template+))) (DOTIMES (i 30)
       ; iterate thru array, randomly set value from {0, 1}
                (SETF (AREF cell i)
                      (RANDOM 2 (MAKE-RANDOM-STATE T))
                )
       ) cell )
)


(DEFUN init_gen (init)
       "Initiate population"
       (DOTIMES (i init)
                (SETF *population* (APPEND (LIST (init_cell)) *population*))
)      )
 

(DEFUN rank_population ()
       "Sort each cell in population by fitness"
       (LET ((temp (SORT *population* #'< :key #'fitness))) 
            (SETF *population* temp)
)      )


(DEFUN offspring (median)
       (SETF *population* (APPEND *population*
           (LET ((i 1) (temp (LIST)) cell) (DOLIST (cell1 *population*)
               (DOLIST (cell2 (NTHCDR i *population*))
                       (SETF cell (splice cell1 cell2 median))
                       (SETF temp (APPEND (LIST cell) temp))
               ) (INCF i)
           ) temp )
       ) )
       (SETF *population* (REMOVE-DUPLICATES *population* :key #'fitness))
)


(DEFUN splice (cell1 cell2 median)
       (LET ((cell (COPY-SEQ +template+)) (current cell1)) (DOTIMES (i 30)
                (WHEN (> i median) (SETF current cell2))
                (SETF (AREF cell i) (AREF current i))
       ) cell )
)


(DEFUN mutate_population (mut)
       ; find number_of_cells
       ; determine how many mutations to be done:
       ;        floor(mutation_rate * number_of_cells)
       (LET* ((pop_count (LENGTH *population*))
              (pop_mut (FLOOR (* mut pop_count)))
              cell_nth gene_nth new_val
             )
             (DOTIMES (i pop_mut)
                   (SETF new_val (RANDOM 2 (MAKE-RANDOM-STATE T)))
                   (SETF cell_nth (RANDOM pop_count (MAKE-RANDOM-STATE T)))
                   (SETF gene_nth (RANDOM 30 (MAKE-RANDOM-STATE T)))
                   (SETF (AREF (NTH cell_nth *population*) gene_nth) new_val)
             ) pop_mut
)      )


(DEFUN selection (pop_cap)
       (SETF *population* (LET ((temp (LIST)) cell) (DOTIMES (i pop_cap)
                (SETF cell (NTH i *population*)) (UNLESS (EQUAL NIL cell)
                     (SETF temp (APPEND temp (LIST cell)))
                )
       ) temp ) )
)


(DEFUN select_smallest (init pop_cap gen_cap &optional median mut_0 mut_1)
       "Main Evolutionary Algorithm"
       ; initiate population
       (UNLESS median (SETF median +default_median+))
       (WHEN (NOT mut_0) (SETF mut_0 +default_mut_0+))
       (WHEN (NOT mut_1) (SETF mut_1 +default_mut_1+))
       (FORMAT T "Initiating population~%")
       (init_gen init) (rank_population)
       (display_population)
       (FORMAT T "~%~%")
       (DOTIMES (i gen_cap)
                (FORMAT T "Offspring ~d of ~d:~%" i gen_cap)
                (mutate_population mut_0) ; mutate cells before mating
                (offspring median) ; mate cells
                (mutate_population mut_1) ; mutate cells after mating
                (rank_population) ; compute fitness to rank cells
                (selection pop_cap) ; save top cells, discard the rest
                (display_population)
                (FORMAT T "~%")
       )        ; repeat for gen_cap times
       ; return the top (one) cell
       (FIRST *population*)
)


(display_cell (select_smallest 5 250 8))


