" Custom vimrc file by timkenhan


filetype plugin on
syntax on

color desert

set nocompatible
set autoindent
set number
set ruler
set hls
set mouse=a
set backspace=2

